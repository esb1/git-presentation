## Getting started

+++

### Enkleste workflow

<table style="font-size: 0.6em">
<tr><td>_-_</td><td>Kun jobbe med gitt i lokalt repo</td></tr>
<tr><td>_-_</td><td>Ingen `git pull/push` i bruk, men resten av kommandoene brukes </td></tr>
<tr><td>_-_</td><td>Kan brukes for å ha "backup" før man gjør endringer</td></tr>
<tr><td>_-_</td><td>Kan brukes (med branches) for å ha parallelle endringer</td></tr>
</table>

+++

### Nytt prosjekt

```
git init
// oppretter et lokalt repository i den mappen du er
```
+++

### Øvelser

<table style="font-size: 0.6em">
<tr><td>_1_</td><td>Installer git: https://www.atlassian.com/git/tutorials/install-git</td></tr>
<tr><td></td><td>(Terminalen fungerer fint på Mac, git-bash på windows)</td></tr>
<tr><td>_2_</td><td>Opprett en mappe, og i mappen kjør `git init`</td></tr>
<tr><td>_3_</td><td>Opprett en fil med innhold og lagre</td></tr>
<tr><td>_4_</td><td>`git add .` og `git commit -m "en passende melding"`</td></tr>
<tr><td>_5_</td><td>`git log`</td></tr>
<tr><td>_6_</td><td>Lag en branch, sjekk den ut og gjenta punkt `3-5`<td></td></tr>
<tr><td>_7_</td><td>Sjekk ut master og merge branchen inn i master</td></tr>
<tr><td></td><td>`git merge <navn på branch>`</td></tr>
<tr><td>_8_</td><td>`git log --oneline --decorate --graph`</td></tr>
<tr><td></td><td>for å se at siste commit er merget</td></tr>
<tr><td>_9_</td><td>Eksperimenter gjerne litt mer med branches og commits</td></tr>
</table>

+++
